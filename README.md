# PlayList_Donwload

Um script para baixar playlists do Youtube. 

## Instalação

Para instalar essa aplicação utilizamos o comando bash

```bash

sudo bash install.sh

```

Use poetry install para verificar se suas dependencias estão instaladas

```bash
poetry install
```

Em seguida damos permissão de execução com: <br>

`chmod +x main.py`

E copiamos o script para o path correto:<br>

`sudo cp ../PlayList_Donwload/ /usr/local/bin/PlayList_Donwload`

Para finalizar criamos um link simbolico para o /bin poder identificar:<br>

`sudo ln -sf /usr/local/bin/PlayList_Donwload/src/main.py /usr/bin/pydownloader`

Desta forma garantimos que o script percorra corretamente

## Features:

1. Baixar playlists no formato de aúdio
2. Baixar playlists no formato de vídeo 
3. Colocar as playlists de forma organizada nas respectivas pastas `$HOME/Música/` e `$HOME/Vídeos/`

### Futuras Atualizações:
  - [x] transformando aúdios .mp4 em .mp3
  - [x] refazer código usando programação orientada a objetos
