#!/bin/bash

install(){
    poetry install
    chmod +x src/main.py 
    cp -r src/ /usr/local/bin/PlayDown
    ln -sf /usr/local/bin/PlayDown/main.py /usr/bin/PlayDown
}

if [ "$(id -u)" -eq 0 ]; then
    install
else
    echo "Você precisa de privilégios de root para executar este script."
fi
