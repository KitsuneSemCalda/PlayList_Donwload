import os,pytube

class Audio:
    def __init__(self,url) -> None:
        self.playlist = pytube.Playlist(url)
        self.musicDir = str(os.getenv("HOME")) + "/Músicas/"
        pass
    
    def running(self):
        os.system("clear")
        print("-"*60 + self.playlist.title + "-" * 60)
    pass

    def endRunning(self):
        print("-"*(60 + len(self.playlist.title)) + "-" * 60)

    def audioDownloader(self):
        self.running()
        counter = 0
        playTitle = self.playlist.owner + "/" + self.playlist.title
        playlist_size = self.playlist.length
        playlist_location = str(self.musicDir) + "/" + playTitle
        for audio in self.playlist.videos:
            counter += 1
            print(f"Baixando...\t{audio.title}")
            try:
                audio.streams.get_by_itag(140).download(playlist_location)
            except Exception:
                audio.streams.get_audio_only().download(playlist_location)
        if (counter < playlist_size):
            print(f"nem todos os audios foram baixados, faltam: {playlist_size - counter}")
        else:
            print(f"todos os {playlist_size} foram baixados com sucesso")
        self.endRunning()
