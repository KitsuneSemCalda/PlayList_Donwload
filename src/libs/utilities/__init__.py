import os
from moviepy.editor import AudioFileClip

music_dir = str(os.getenv("HOME")) + "/Música"

def converterMp4toMp3():
    for directory in os.listdir(music_dir):
        input_dir = music_dir
        input_dir += f"/{directory}" 
        for subdirectory in os.listdir(directory):
            input_dir += f"/{subdirectory}"
            for item in os.listdir(input_dir):
                filename,fileextension = os.path.splitext(item)
                if fileextension == ".mp4":
                    mp4_file = input_dir + f"/{item}"
                    mp3_file = input_dir + filename + ".mp3"

                    audio_clip = AudioFileClip(mp4_file)
                    audio_clip.write_audiofile(mp3_file)

                    audio_clip.close()
                    os.remove(mp4_file)
        input_dir = music_dir
