import pytube,os

class Video:
    def __init__(self,url) -> None:
        self.playlist = pytube.Playlist(url)
        self.videoDir = str(os.getenv("HOME")) + "/Vídeos"
        pass

    def running(self):
        os.system("clear")
        print("-" * 60 + self.playlist.title + "-"*60)
    pass 
    
    def endRunning(self):
        print("-" * (60 + len(self.playlist.title)) + "-" * 60)

    def videoDownloader(self) -> None:
        self.running()
        counter = 0
        playTitle = self.playlist.owner + "/" + self.playlist.title
        playlist_size = self.playlist.length
        playlist_location = str(self.videoDir) + f"/{playTitle}"
        for video in self.playlist.videos:
            counter += 1
            print(f"baixando...\t{video.title}")
            try:
                video.streams.get_by_itag(22).download(playlist_location)
            except Exception:
                video.streams.get_highest_resolution().download(playlist_location)
        if (counter < playlist_size):
            print(f"nem todos os vídeos foram baixados faltam: {playlist_size - counter}")
        else:
            print(f"todos os {playlist_size} foram baixados com sucesso")
        self.endRunning()
pass
