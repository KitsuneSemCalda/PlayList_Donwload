#!/usr/bin/env python3
from libs import video_downloader 
from libs import audio_downloader
from libs import utilities

if __name__ == "__main__":
    download_type = input("qual o tipo de playlist que você deseja baixar (audio ou video):  ")
    if download_type == "audio":
        url = input("Digite a url da playlist aqui: ")
        audio_downloader.Audio(url).audioDownloader()
        utilities.converterMp4toMp3()
    elif download_type == "video":
        url = input("Digite a url da playlist aqui: ")
        video_downloader.Video(url).videoDownloader()
